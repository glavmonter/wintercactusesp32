# WinterCactus

Код для управления климатом зимовального ящика.

Корневой сертификат Let's Encrypt взят с сайта <https://letsencrypt.org/ru/certificates/>. 
MQTT with TLS on port 8883.


# Настройка и сборка

## CLion & Windows

Установка ESP-IDF.

1. Устанавливаем python3 v3.9.6 в переменные среды. Из консоли cmd должен вызываться питон
2. Клонируем ESP-IDF `git clone --recursive https://github.com/espressif/esp-idf.git`
3. Настраиваем `cd esp-idf && install.bat`. Скачивается компиллятор, прочее барахло, настраивается venv питона в ~/.espressif/python_env/
4. Запускаем `export.bat`. Экспортирует необходимые пути в PATH, создаёт переменные окружения
5. Переходим в нужный example, `idf.py build` для сборки, `idf.py -p PORT flash` для прошивки, `idf.py monitor` для монитора COM порта


Настройка CLion

Неплохое [видео](https://www.youtube.com/watch?v=M6fa7tzZdLw) и [мануал](https://www.jetbrains.com/help/clion/esp-idf.html).

1. Настроить toolchain на MinGW. cmake лучше прописать из ~/.espressif/tools/cmake
2. Добавляем пути в Settings->CMake->Environment:
    1. IDF_PATH -> \<path_to_idf\>
    2. В системных Path в начало добавляем пути, с .espressif, которые появляются после export.bat
3. В CMake options: `-G Ninja -DESP_PLATFORM=1 -DIDF_TARGET=esp32 -DCCACHE_ENABLE=1 -DPYTHON=~/.espressif/python_env/...../Scripts/python.exe`
4. Очищаем кэш cmake и собираем


Добавление дополнительных модулей

1. Клонируем <https://github.com/UncleRus/esp-idf-lib.git> рядом с ESP-IDF
2. Удаляем лишнее в components кроме нужного блока и esp_idf_lib_helpers.
3. В верхнем CMakeLists.txt добавляем `set(EXTRA_COMPONENT_DIRS $ENV{IDF_PATH}/../esp-idf-lib/components)`


# Датчики DS18B20

| Номер |       Адрес       | Примечание |
|:-----:|:-----------------:|------------|
|   1   | 8E3C01F09584E328  |            |
|   2   | 403C01F09641C328  |            |
|   3   | 553C01F09575F628  |            |
|   4   | EC3C01F0953ABB28  |            |
