menu "WinterCactus WiFi Connection Configuration"

    config WINTERCACTUS_WIFI_SSID
        string "WiFi SSID"
        default "myssid"
        help
            SSID (network name) for the example to connect to.

    config WINTERCACTUS_WIFI_PASSWORD
        string "WiFi Password"
        default "mypassword"
        help
            WiFi password (WPA or WPA2) for the example to use.
            Can be left blank if the network has no security set.

    choice WINTERCACTUS_WIFI_SCAN_METHOD
        prompt "WiFi Scan Method"
        default WINTERCACTUS_WIFI_SCAN_METHOD_ALL_CHANNEL
        help
            WiFi scan method:

            If "Fast" is selected, scan will end after find SSID match AP.

            If "All Channel" is selected, scan will end after scan all the channel.

        config WINTERCACTUS_WIFI_SCAN_METHOD_FAST
            bool "Fast"
        config WINTERCACTUS_WIFI_SCAN_METHOD_ALL_CHANNEL
            bool "All Channel"
    endchoice

    menu "WiFi Scan threshold"
        config WINTERCACTUS_WIFI_SCAN_RSSI_THRESHOLD
            int "WiFi minimum rssi"
            range -127 0

            default -127
            help
                The minimum rssi to accept in the scan mode.

        choice WINTERCACTUS_WIFI_SCAN_AUTH_MODE_THRESHOLD
            prompt "WiFi Scan auth mode threshold"
            default WINTERCACTUS_WIFI_AUTH_OPEN
            help
                The weakest authmode to accept in the scan mode.

            config WINTERCACTUS_WIFI_AUTH_OPEN
                bool "OPEN"
            config WINTERCACTUS_WIFI_AUTH_WEP
                bool "WEP"
            config WINTERCACTUS_WIFI_AUTH_WPA_PSK
                bool "WPA PSK"
            config WINTERCACTUS_WIFI_AUTH_WPA2_PSK
                bool "WPA2 PSK"
            config WINTERCACTUS_WIFI_AUTH_WPA_WPA2_PSK
                bool "WPA WPA2 PSK"
            config WINTERCACTUS_WIFI_AUTH_WPA2_ENTERPRISE
                bool "WPA2 ENTERPRISE"
            config WINTERCACTUS_WIFI_AUTH_WPA3_PSK
                bool "WPA3 PSK"
            config WINTERCACTUS_WIFI_AUTH_WPA2_WPA3_PSK
                bool "WPA2 WPA3 PSK"
            config WINTERCACTUS_WIFI_AUTH_WAPI_PSK
                bool "WAPI PSK"
        endchoice
    endmenu

    choice WINTERCACTUS_WIFI_CONNECT_AP_SORT_METHOD
        prompt "WiFi Connect AP Sort Method"
        default WINTERCACTUS_WIFI_CONNECT_AP_BY_SIGNAL
        help
            WiFi connect AP sort method:

            If "Signal" is selected, Sort matched APs in scan list by RSSI.

            If "Security" is selected, Sort matched APs in scan list by security mode.

        config WINTERCACTUS_WIFI_CONNECT_AP_BY_SIGNAL
            bool "Signal"
        config WINTERCACTUS_WIFI_CONNECT_AP_BY_SECURITY
            bool "Security"
    endchoice

    config WINTERCACTUS_CONNECT_IPV6
        bool "Obtain IPv6 address"
        default n
        select LWIP_IPV6
        help
            By default, examples will wait until IPv4 and IPv6 local link addresses are obtained.
            Disable this option if the network does not support IPv6.
            Choose the preferred IPv6 address type if the connection code should wait until other than
            the local link address gets assigned.
            Consider enabling IPv6 stateless address autoconfiguration (SLAAC) in the LWIP component.

    if WINTERCACTUS_CONNECT_IPV6
        choice WINTERCACTUS_CONNECT_PREFERRED_IPV6
            prompt "Preferred IPv6 Type"
            default WINTERCACTUS_CONNECT_IPV6_PREF_LOCAL_LINK
            help
                Select which kind of IPv6 address the connect logic waits for.

            config WINTERCACTUS_CONNECT_IPV6_PREF_LOCAL_LINK
                bool "Local Link Address"
                help
                    Blocks until Local link address assigned.

            config WINTERCACTUS_CONNECT_IPV6_PREF_GLOBAL
                bool "Global Address"
                help
                    Blocks until Global address assigned.

            config WINTERCACTUS_CONNECT_IPV6_PREF_SITE_LOCAL
                bool "Site Local Address"
                help
                    Blocks until Site link address assigned.

            config WINTERCACTUS_CONNECT_IPV6_PREF_UNIQUE_LOCAL
                bool "Unique Local Link Address"
                help
                    Blocks until Unique local address assigned.

        endchoice

    endif # WINTERCACTUS_CONNECT_IPV6

endmenu


menu "WinterCactus Configuration"

    config MQTT_BROKER_URL
        string "MQTT Broker URL"
        default "mqtt://mqtt.your_site.io"
        help
            URL of the broker to connect to

    config MQTT_USERNAME
        string "MQTT Username"
        default "none"

    config MQTT_PASSWORD
        string "MQTT User Password"
        default "none"

endmenu
