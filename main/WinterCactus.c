#include <stdio.h>
#include "nvs_flash.h"
#include "esp_netif.h"
#include "esp_log.h"
#include "connect_manager.h"
#include "mqtt_client.h"
#include "driver/gpio.h"
#include "esp_task_wdt.h"
#include "dht.h"
#include <ds18x20.h>


static const char *TAG = "WINC";
static const char *TAG_DHT = "dht";

static esp_mqtt_client_handle_t mqtt_client;

extern const uint8_t letsencrypt_root_cert_pem_start[]   asm("_binary_letsencrypt_root_cert_pem_start");
extern const uint8_t letsencrypt_root_cert_pem_end[]   asm("_binary_letsencrypt_root_cert_pem_end");


#define DHT22_GPIO_PIN          GPIO_NUM_26
#define DS18B20_GPIO_PIN        GPIO_NUM_25

#define RELAY_1_GPIO_PIN        GPIO_NUM_14
#define RELAY_2_GPIO_PIN        GPIO_NUM_27
#define RELAY_3_GPIO_PIN        GPIO_NUM_16
#define RELAY_4_GPIO_PIN        GPIO_NUM_17
#define LED_GPIO_PIN            GPIO_NUM_2

#define THERMOSTAT_SENSOR_DEFAULT  "/ds/8E3C01F09584E328"
#define THERMOSTAT_SENSOR_SIZE      (sizeof(THERMOSTAT_SENSOR_DEFAULT) + 5)
#define THERMOSTAT_RELAY_DEFAULT   (1)
#define LIGHT_RELAY_DEFAULT         (4)


static void log_error_if_nonzero(const char *message, int error_code)
{
    if (error_code != 0) {
        ESP_LOGE(TAG, "Last error %s: 0x%x", message, error_code);
    }
}

SemaphoreHandle_t mutex_gpio;
QueueHandle_t queue_update_settings;
QueueHandle_t queue_wdt;

int alarm_val = 1;

static void SetRelay(int relay, int state) {
    if (!((state == 0) || (state == 1)))
        return;

    char topic[] = "/relay/state/0";
    char data;

    ESP_LOGD(TAG, "Set Relay %d to %d", relay, state);
    int level = state == 0 ? 0 : 1;
    data = level + '0';

    switch (relay) {
        case 1:
            gpio_set_level(RELAY_1_GPIO_PIN, level);
            topic[13] = '1';
            esp_mqtt_client_publish(mqtt_client, topic, &data, 1, 0, 0);
            break;
        case 2:
            gpio_set_level(RELAY_2_GPIO_PIN, level);
            topic[13] = '2';
            esp_mqtt_client_publish(mqtt_client, topic, &data, 1, 0, 0);
            break;
        case 3:
            gpio_set_level(RELAY_3_GPIO_PIN, level);
            topic[13] = '3';
            esp_mqtt_client_publish(mqtt_client, topic, &data, 1, 0, 0);
            break;
        case 4:
            gpio_set_level(RELAY_4_GPIO_PIN, level);
            topic[13] = '4';
            esp_mqtt_client_publish(mqtt_client, topic, &data, 1, 0, 0);
            break;
        default: return;
    }
}

static int relay_state(const char *data, int data_len) {
    if (data_len != 1)
        return -1;

    if (strncmp(data, "0", data_len) == 0) {
        return 0;
    } else if (strncmp(data, "1", data_len) == 0) {
        return 1;
    } else {
        return -1;
    }
}

static void update_relay_state(const char *topic, int topic_len, const char *data, int data_len) {
    if (strncmp(topic, "/relay/", 7) != 0)
        return;

    int ret = relay_state(data, data_len);
    ESP_LOGD(TAG, "Relay state: %d", ret);
    if (ret < 0)
        return;

    if (strncmp(topic, "/relay/1", topic_len) == 0) {
        SetRelay(1, ret);
        return;
    }
    if (strncmp(topic, "/relay/2", topic_len) == 0) {
        SetRelay(2, ret);
        return;
    }
    if (strncmp(topic, "/relay/3", topic_len) == 0) {
        SetRelay(3, ret);
        return;
    }
    if (strncmp(topic, "/relay/4", topic_len) == 0) {
        SetRelay(4, ret);
        return;
    }
}


esp_err_t UpdateNvsRecord_i8(nvs_handle_t nvsHandle, const char *key, int8_t value, bool *need_update) {
    int8_t readback = 0;
    *need_update = false;
    esp_err_t err = nvs_get_i8(nvsHandle, key, &readback);
    if (err == ESP_OK) {
        if (readback != value) {
            ESP_LOGI(TAG, "Update NVS i8 value with key %s", key);
            err = nvs_set_i8(nvsHandle, key, value);
            if (err == ESP_OK) {
                *need_update = true;
                return ESP_OK;
            }
        } else {
            ESP_LOGD(TAG, "Value not changed, exiting OK");
            *need_update = false;
            return ESP_OK;
        }
    } else if (err == ESP_ERR_NVS_NOT_FOUND) {
        err = nvs_set_i8(nvsHandle, key, value);
        ESP_LOGI(TAG, "Create new `%s` record with %d -> %s", key, value, esp_err_to_name(err));
        if (err == ESP_OK) {
            *need_update = true;
            return ESP_OK;
        }
    }
    ESP_LOGE(TAG, "Update NVS i8 (%s) Failed with %s", key, esp_err_to_name(err));
    return err;
}

esp_err_t UpdateNvsRecord_blob(nvs_handle_t nvsHandle, const char *key, const char *value, bool *need_update) {
    esp_err_t err;
    char lb[THERMOSTAT_SENSOR_SIZE];
    ESP_LOGD(TAG, "Update NVS blob %s", key);
    *need_update = false;
    memset(lb, 0, sizeof(lb));
    size_t required_size = 0;

    err = nvs_get_blob(nvsHandle, key, NULL, &required_size);
    if (err == ESP_OK) {
        ESP_LOGD(TAG, "Found %zu bytes blob", required_size);
        if (strlen(value) != required_size) {
            ESP_LOGI(TAG, "Record len mismatch, update it!");
            *need_update = true;
        } else {
            err = nvs_get_blob(nvsHandle, key, lb, &required_size);
            if (err == ESP_OK) {
                int cmp = strncmp(lb, value, required_size);
                ESP_LOGD(TAG, "Compare %zu bytes. Result %d", required_size, cmp);
                if (cmp != 0) {
                    *need_update = true;
                } else {
                    ESP_LOGD(TAG, "Values equivalent");
                    *need_update = false;
                    return ESP_OK;
                }
            }
        }
    } else if (err == ESP_ERR_NVS_NOT_FOUND) {
        ESP_LOGI(TAG, "Record %s not found", key);
        *need_update = true;
    }

    if (*need_update == true) {
        ESP_LOGI(TAG, "Write new or update last BLOB");
        err = nvs_set_blob(nvsHandle, key, value, strlen(value));
        if (err == ESP_OK) {
            return ESP_OK;
        }
    }
    ESP_LOGE(TAG, "Update NVS blob (%s) Failed with %s", key, esp_err_to_name(err));
    return err;
}

static esp_err_t UpdateLight(const char *topic, int topic_len, const char *data, int data_len) {
    if (strncmp(topic, "/light/", 7) != 0)
        return ESP_OK;

    esp_err_t err;
    nvs_handle_t nvsHandle;
    err = nvs_open("storage", NVS_READWRITE, &nvsHandle);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Cannot open NVS: %s", esp_err_to_name(err));
    }

    char buf[THERMOSTAT_SENSOR_SIZE];
    bool need_update = false;
    int8_t relay = -1;
    if (strncmp(topic, "/light/relay", topic_len) == 0) {
        memset(buf, 0, sizeof(buf));
        strncpy(buf, data, data_len);
        int new_relay = strtol(buf, NULL, 10);
        ESP_LOGI(TAG, "Light relay: %d", new_relay);
        err = UpdateNvsRecord_i8(nvsHandle, "light", (int8_t)new_relay, &need_update);
        ESP_LOGI(TAG, "Update Light relay!");
    }
    if (need_update) {
        ESP_LOGI(TAG, "Committing NVS changes....");
        err = nvs_commit(nvsHandle);
        if (err != ESP_OK) {
            ESP_LOGE(TAG, "Cannot commit NVS: %s", esp_err_to_name(err));
        }
    }
    err = nvs_get_i8(nvsHandle, "light", &relay);
    if (err != ESP_OK) {
        relay = LIGHT_RELAY_DEFAULT;
    }
    nvs_close(nvsHandle);

    if (strncmp(topic, "/light/state", topic_len) == 0) {
        if (data_len != 1) {
            ESP_LOGW(TAG, "Light state data size invalid");
            return ESP_OK;
        }
        if (data[0] == '0') {
            ESP_LOGI(TAG, "Reset light relay %d", relay);
            SetRelay(relay, 0);
        } else if (data[0] == '1') {
            ESP_LOGI(TAG, "Set light relay %d", relay);
            SetRelay(relay, 1);
        }
    }

    return ESP_OK;
}


static esp_err_t UpdateThermostat(const char *topic, int topic_len, const char *data, int data_len) {
    if (strncmp(topic, "/thermo/", 8) != 0)
        return ESP_OK;
    ESP_LOGD(TAG, "/thermo/ topic!!!");
    char buf[THERMOSTAT_SENSOR_SIZE];

    bool need_update = false;

    nvs_handle_t nvsHandle;
    esp_err_t err = nvs_open("storage", NVS_READWRITE, &nvsHandle);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Cannot open NVS: %s", esp_err_to_name(err));
    }

    // TODO проверка на data_len
    if (strncmp(topic, "/thermo/ton", topic_len) == 0) {
        memset(buf, 0, sizeof(buf));
        strncpy(buf, data, data_len);
        int ton = strtol(buf, NULL, 10);
        ESP_LOGD(TAG, "Ton %d", ton);
        err = UpdateNvsRecord_i8(nvsHandle, "ton", (int8_t)ton, &need_update);
        ESP_LOGI(TAG, "Update TON: %s", esp_err_to_name(err));
    }

    if (strncmp(topic, "/thermo/toff", topic_len) == 0) {
        memset(buf, 0, sizeof(buf));
        strncpy(buf, data, data_len);
        int toff = strtol(buf, NULL, 10);
        ESP_LOGD(TAG, "Toff %d", toff);
        err = UpdateNvsRecord_i8(nvsHandle, "toff", (int8_t)toff, &need_update);
        ESP_LOGI(TAG, "Update TOFF: %s", esp_err_to_name(err));
    }

    if (strncmp(topic, "/thermo/relay", topic_len) == 0) {
        memset(buf, 0, sizeof(buf));
        strncpy(buf, data, data_len);
        int relay = strtol(buf, NULL, 10);
        ESP_LOGD(TAG, "Relay %d", relay);
        err = UpdateNvsRecord_i8(nvsHandle, "relay", (int8_t)relay, &need_update);
        ESP_LOGI(TAG, "Update RELAY: %s", esp_err_to_name(err));
    }

    if (strncmp(topic, "/thermo/sensor", topic_len) == 0) {
        memset(buf, 0, sizeof(buf));
        strncpy(buf, data, data_len);
        ESP_LOGD(TAG, "Sensor %s", buf);
        err = UpdateNvsRecord_blob(nvsHandle, "sensor", buf, &need_update);
        ESP_LOGI(TAG, "Update SENSOR: %s", esp_err_to_name(err));
    }

    if (need_update) {
        ESP_LOGD(TAG, "Committing NVS changes....");
        err = nvs_commit(nvsHandle);
        if (err != ESP_OK) {
            ESP_LOGE(TAG, "Cannot commit NVS: %s", esp_err_to_name(err));
        }
        uint32_t up = 1;
        xQueueOverwrite(queue_update_settings, &up);
    }
    nvs_close(nvsHandle);
    return ESP_OK;
}

/*
 * @brief Event handler registered to receive MQTT events
 *
 *  This function is called by the MQTT client event loop.
 *
 * @param handler_args user data registered to the event.
 * @param base Event base for the handler(always MQTT Base in this example).
 * @param event_id The id for the received event.
 * @param event_data The data for the event, esp_mqtt_event_handle_t.
 */
static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    esp_mqtt_event_handle_t event = event_data;
    esp_mqtt_client_handle_t client = event->client;
    int msg_id;
    switch ((esp_mqtt_event_id_t)event_id) {
        case MQTT_EVENT_CONNECTED:
            msg_id = esp_mqtt_client_subscribe(client, "/thermo/ton", 1);
            ESP_LOGI(TAG, "Subscribe to /thermo/ton, msg_id=%d", msg_id);
            msg_id = esp_mqtt_client_subscribe(client, "/thermo/toff", 1);
            ESP_LOGI(TAG, "Subscribe to /thermo/toff, msg_id=%d", msg_id);
            msg_id = esp_mqtt_client_subscribe(client, "/thermo/relay", 1);
            ESP_LOGI(TAG, "Subscribe to /thermo/relay, msg_id=%d", msg_id);
            msg_id = esp_mqtt_client_subscribe(client, "/thermo/sensor", 1);
            ESP_LOGI(TAG, "Subscribe to /thermo/sensor, msg_id=%d", msg_id);
            msg_id = esp_mqtt_client_subscribe(client, "/thermo/alarm_val", 1);
            ESP_LOGI(TAG, "Subscribe to /thermo/alarm_val, msg_id=%d", msg_id);

            msg_id = esp_mqtt_client_subscribe(client, "/light/relay", 1);
            ESP_LOGI(TAG, "Subscribe to /light/relay, msg_id=%d", msg_id);
            msg_id = esp_mqtt_client_subscribe(client, "/light/state", 1);
            ESP_LOGI(TAG, "Subscribe to /light/state, msg_id=%d", msg_id);

            msg_id = esp_mqtt_client_subscribe(client, "/wdt", 0);
            ESP_LOGI(TAG, "Subscribe to /wdt, msg_id=%d", msg_id);
            break;

        case MQTT_EVENT_DATA:
            ESP_LOGI(TAG, "MQTT_EVENT_DATA");
            if (xSemaphoreTake(mutex_gpio, portMAX_DELAY) == pdTRUE) {
                update_relay_state(event->topic, event->topic_len, event->data, event->data_len);
                xSemaphoreGive(mutex_gpio);
            }

            UpdateThermostat(event->topic, event->topic_len, event->data, event->data_len);
            UpdateLight(event->topic, event->topic_len, event->data, event->data_len);
            if (strncmp("/wdt", event->topic, event->topic_len) == 0) {
                static uint32_t val = 0;
                xQueueSend(queue_wdt, &val, 10);
                val += 1;
            }

            if (strncmp("/thermo/alarm_val", event->topic, event->topic_len) == 0) {
                if (event->data_len == 1) {
                    alarm_val = event->data[0] - '0';
                    ESP_LOGI(TAG, "Set alarm relay value to %d", alarm_val);
                }
            }
            break;

        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            if (event->error_handle->error_type == MQTT_ERROR_TYPE_TCP_TRANSPORT) {
                log_error_if_nonzero("reported from esp-tls", event->error_handle->esp_tls_last_esp_err);
                log_error_if_nonzero("reported from tls stack", event->error_handle->esp_tls_stack_err);
                log_error_if_nonzero("captured as transport's socket errno",  event->error_handle->esp_transport_sock_errno);
                ESP_LOGI(TAG, "Last errno string (%s)", strerror(event->error_handle->esp_transport_sock_errno));
            }
            break;
        default:
            ESP_LOGI(TAG, "Other event id:%d", event->event_id);
            break;
    }
}




static void mqtt_app_start() {
    esp_mqtt_client_config_t mqtt_cfg = {
            .uri = CONFIG_MQTT_BROKER_URL,
            .username = CONFIG_MQTT_USERNAME,
            .password = CONFIG_MQTT_PASSWORD,
            .cert_pem = (const char *)letsencrypt_root_cert_pem_start,
    };

    mqtt_client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_register_event(mqtt_client, ESP_EVENT_ANY_ID, mqtt_event_handler, NULL);
    esp_mqtt_client_start(mqtt_client);
}

static int8_t low_sign(int16_t val) {
    int16_t h = val / 10;
    return val - h * 10;
}



esp_err_t LoadSettings(int8_t *ton, int8_t *toff, char *sensor, int8_t *relay) {
nvs_handle_t nvsHandle;
esp_err_t err;

    err = nvs_open("storage", NVS_READWRITE, &nvsHandle);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Error (%s) opening NVS", esp_err_to_name(err));
        return err;
    }
    err = nvs_get_i8(nvsHandle, "ton", ton);
    if (err != ESP_OK) {
        ESP_LOGW(TAG, "Load default ton (%s)", esp_err_to_name(err));
        *ton = 10;
    }
    err = nvs_get_i8(nvsHandle, "toff", toff);
    if (err != ESP_OK) {
        ESP_LOGW(TAG, "Load default toff (%s)", esp_err_to_name(err));
        *toff = 12;
    }

    err = nvs_get_i8(nvsHandle, "relay", relay);
    if (err != ESP_OK) {
        ESP_LOGW(TAG, "Load default relay (%s)", esp_err_to_name(err));
        *relay = THERMOSTAT_RELAY_DEFAULT;
    }

    size_t required_size = 0;
    err = nvs_get_blob(nvsHandle, "sensor", NULL, &required_size);
    if (err != ESP_OK && err != ESP_ERR_NVS_NOT_FOUND) {
        required_size = 0;
    }
    if (required_size == 0) {
        ESP_LOGW(TAG, "Load default sensor address");
        strcpy(sensor, THERMOSTAT_SENSOR_DEFAULT);
    } else if (required_size > sizeof(THERMOSTAT_SENSOR_DEFAULT)) {
        ESP_LOGE(TAG, "Sensor name to long. Load default");
        strcpy(sensor, THERMOSTAT_SENSOR_DEFAULT);
    } else {
        ESP_LOGD(TAG, "Load sensor name, size %zu", required_size);
        err = nvs_get_blob(nvsHandle, "sensor", sensor, &required_size);
        if (err != ESP_OK) {
            ESP_LOGE(TAG, "Cannot load sensor name: %s", esp_err_to_name(err));
            strcpy(sensor, THERMOSTAT_SENSOR_DEFAULT);
        }
    }

    nvs_close(nvsHandle);
    return err;
}


static const char *TAG_DS = "tstat";

static bool thermo_reg_on = false;
void ProcessRegulator(int8_t on, int8_t off, float current, int8_t relay) {
    ESP_LOGI(TAG_DS, "Process regulator");
    if (thermo_reg_on == false) {
        // Отключен
        SetRelay(relay, 0);
        if (current < (float)on) {
            ESP_LOGI(TAG, "Enable Heater");
            thermo_reg_on = true;
            SetRelay(relay, 1);
        }
    } else {
        SetRelay(relay, 1);
        if (current > (float)off) {
            ESP_LOGI(TAG, "Disable Heater");
            thermo_reg_on = false;
            SetRelay(relay, 0);
        }
    }
}


static const uint32_t LOOP_DELAY_MS = 10000;
static const int MAX_SENSORS = 8;
static const int RESCAN_INTERVAL = 1;


void thermostat_task(void *pvParameters) {
    (void)pvParameters;
    ds18x20_addr_t addrs[MAX_SENSORS];
    float temps[MAX_SENSORS];
    size_t sensor_count = 0;
    char topic_name[32];
    char topic_data[64];

    vTaskDelay(pdMS_TO_TICKS(250));

    int8_t ton, toff;
    int8_t relay;
    char sensor[THERMOSTAT_SENSOR_SIZE];
    memset(sensor, 0, sizeof(sensor));
    LoadSettings(&ton, &toff, sensor, &relay);
    ESP_LOGI(TAG, "Ton:    %d C", ton);
    ESP_LOGI(TAG, "Toff:   %d C", toff);
    ESP_LOGI(TAG, "Relay:  %d", relay);
    ESP_LOGI(TAG, "Sensor: %s", sensor);

    esp_err_t res = ESP_OK;
    int32_t update_value;

    esp_task_wdt_add(NULL);

    for (;;) {
        esp_task_wdt_reset();

        if (xQueueReceive(queue_update_settings, &update_value, 5) == pdTRUE) {
            ESP_LOGI(TAG, "Reread settings");
            memset(sensor, 0, sizeof(sensor));
            LoadSettings(&ton, &toff, sensor, &relay);
            ESP_LOGI(TAG, "Ton:    %d C", ton);
            ESP_LOGI(TAG, "Toff:   %d C", toff);
            ESP_LOGI(TAG, "Relay:  %d", relay);
            ESP_LOGI(TAG, "Sensor: %s", sensor);
        }

        if (xSemaphoreTake(mutex_gpio, portMAX_DELAY) == pdTRUE) {
            res = ds18x20_scan_devices(DS18B20_GPIO_PIN, addrs, MAX_SENSORS, &sensor_count);
            xSemaphoreGive(mutex_gpio);
        }
        if (res != ESP_OK) {
            ESP_LOGE(TAG_DS, "Sensor scan error %d (%s)", res, esp_err_to_name(res));
            vTaskDelay(pdMS_TO_TICKS(LOOP_DELAY_MS));
            continue;
        }

        if (!sensor_count) {
            ESP_LOGW(TAG_DS, "No sensors detected");
            vTaskDelay(pdMS_TO_TICKS(LOOP_DELAY_MS));
            continue;
        }

        ESP_LOGI(TAG_DS, "%zu sensors detected", sensor_count);
        if (sensor_count > MAX_SENSORS) {
            sensor_count = MAX_SENSORS;
        }

        for (int i = 0; i < RESCAN_INTERVAL; i++) {
            ESP_LOGI(TAG_DS, "Measuring...");
            if (xSemaphoreTake(mutex_gpio, portMAX_DELAY) == pdTRUE) {
                res = ds18x20_measure_and_read_multi(DS18B20_GPIO_PIN, addrs, sensor_count, temps);
                xSemaphoreGive(mutex_gpio);
            }
            if (res != ESP_OK) {
                ESP_LOGE(TAG_DS, "Sensor read error %d (%s)", res, esp_err_to_name(res));
                continue;
            }

            bool sensor_valid = false;
            for (int s = 0; s < sensor_count; s++) {
                float temp_c = temps[s];
                snprintf(topic_name, sizeof(topic_name), "/ds/%08X%08X", (uint32_t)(addrs[s] >> 32), (uint32_t)addrs[s]);
                snprintf(topic_data, sizeof(topic_data) - 1, "{\"status\":\"ok\",\"t\":%.3f,\"h\":0}", temp_c);
                esp_mqtt_client_publish(mqtt_client, topic_name, topic_data, strlen(topic_data), 0, 0);

                ESP_LOGI(TAG_DS, "Sensor %08x%08x (%s) reports %.3f C",
                         (uint32_t)(addrs[s] >> 32), (uint32_t)addrs[s],
                         (addrs[s] & 0xFF) == DS18B20_FAMILY_ID ? "DS18B20" : "DS18S20",
                         temp_c);

                if (strncmp(topic_name, sensor, strlen(sensor)) == 0) {
                    sensor_valid = true;
                    ProcessRegulator(ton, toff, temp_c, relay);
                }
            }
            if (!sensor_valid) {
                ESP_LOGW(TAG_DS, "No regulator sensor found, Disable heater!!");
                SetRelay(relay, alarm_val);
                esp_mqtt_client_publish(mqtt_client, "/thermo/alarm", "1", 1, 2, 0);
            } else {
                esp_mqtt_client_publish(mqtt_client, "/thermo/alarm", "0", 1, 2, 0);
            }
            vTaskDelay(pdMS_TO_TICKS(LOOP_DELAY_MS));
        }
    }
}


void dht_task(void *arg) {
    (void)arg;

    int16_t t = 0, h = 0;
    char buf[64];

    for (;;) {
        if (xSemaphoreTake(mutex_gpio, portMAX_DELAY) == pdTRUE) {
            esp_err_t err = dht_read_data(DHT_TYPE_AM2301, DHT22_GPIO_PIN, &h, &t);
            if (err == ESP_OK) {
                ESP_LOGD(TAG_DHT, "Raw T: %d, Raw H: %d", t, h);
                ESP_LOGD(TAG_DHT, "T: %d.%d C, H: %d.%d%%", t / 10, low_sign(t), h / 10, low_sign(h));

                snprintf(buf, sizeof(buf) - 1, "{\"status\":\"ok\",\"t\":%d,\"h\":%d}", t, h);
                esp_mqtt_client_publish(mqtt_client, "/dht1", buf, strlen(buf), 0, 0);
            }
            xSemaphoreGive(mutex_gpio);
        }
        vTaskDelay(10000 / portTICK_PERIOD_MS);
    }
}


void wdt_task(void *arg) {
    esp_task_wdt_add(NULL);
    ESP_LOGI("WDT", "Start WDT task");
    uint32_t val;
    char buf[16];
    uint32_t led_state = 0;
    for (;;) {
        if (xQueueReceive(queue_wdt, &val, pdMS_TO_TICKS(100)) == pdTRUE) {
            esp_task_wdt_reset();
            ESP_LOGI("WDT", "Reset WDT: %u", val);
            snprintf(buf, sizeof(buf), "%d", val);
            esp_mqtt_client_publish(mqtt_client, "/wdt_rb", buf, strlen(buf), 0, 0);
        } else {
            if (xSemaphoreTake(mutex_gpio, 100)) {
                gpio_set_level(LED_GPIO_PIN, led_state % 2);
                led_state++;
                xSemaphoreGive(mutex_gpio);
            }
        }
    }
}


void app_main(void)
{
    esp_task_wdt_init(60 * 8, true); // 8 минут вачдог на обновление, потом сброс

    ESP_LOGI(TAG, "[APP] Startup..");
    ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

    esp_log_level_set("*", ESP_LOG_INFO);
    esp_log_level_set("MQTT_CLIENT", ESP_LOG_VERBOSE);
    esp_log_level_set("MQTT_EXAMPLE", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_BASE", ESP_LOG_VERBOSE);
    esp_log_level_set("esp-tls", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT", ESP_LOG_VERBOSE);
    esp_log_level_set("OUTBOX", ESP_LOG_VERBOSE);
    esp_log_level_set(TAG, ESP_LOG_DEBUG);

    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);

    queue_wdt = xQueueCreate(1, sizeof(uint32_t));
    queue_update_settings = xQueueCreate(1, sizeof(uint32_t));

    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    mutex_gpio = xSemaphoreCreateMutex();

    gpio_config_t io_conf = {};
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = (1 << RELAY_1_GPIO_PIN) | (1 << RELAY_2_GPIO_PIN) | (1 << RELAY_3_GPIO_PIN) | (1 << RELAY_4_GPIO_PIN) | (1 << LED_GPIO_PIN);
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 0;
    gpio_config(&io_conf);
    gpio_set_level(RELAY_1_GPIO_PIN, 0);
    gpio_set_level(RELAY_2_GPIO_PIN, 0);
    gpio_set_level(RELAY_3_GPIO_PIN, 0);
    gpio_set_level(RELAY_4_GPIO_PIN, 0);

    TaskHandle_t task_handle;
    xTaskCreatePinnedToCore(wdt_task, "wdt", configMINIMAL_STACK_SIZE * 7, NULL, 1, &task_handle, APP_CPU_NUM);
    if (task_handle == NULL) {
        ESP_ERROR_CHECK(ESP_ERR_NO_MEM);
    }

    xTaskCreatePinnedToCore(thermostat_task, "thermo", configMINIMAL_STACK_SIZE * 7, NULL, 2, &task_handle, APP_CPU_NUM);
    if (task_handle == NULL) {
        ESP_ERROR_CHECK(ESP_ERR_NO_MEM);
    }

    xTaskCreatePinnedToCore(dht_task, "dth", configMINIMAL_STACK_SIZE * 7, NULL, 2, &task_handle, 1);
    if (task_handle == NULL) {
        ESP_ERROR_CHECK(ESP_ERR_NO_MEM);
    }
    ESP_ERROR_CHECK(manager_connect());
    mqtt_app_start();
}
